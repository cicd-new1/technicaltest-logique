**Answer :**

**to start using this script you need :**

A.	Aws account ( with the access that has been granted)

B.	Terraform installed on your local

C.	aws-cli installed on your local

D.	clone my repository 


**for this case :**

**A.	Case:**

We need to deploy a “Food Ordering” System based on this requirements:

i. Programming language

ii. Frontend: React.js

iii. Backend: Node.js

iv. Mobile app: React Native

v. Database: PostgreSQL

**B.	Server requirements**


i. Microservices using kubernetes (total: 5 services) and service mesh

ii. Provide secure access into resources and service either from internal or external traffic

iii. Analytic which capturing, analyzing, and securely storing transaction log data

iv. CI/CD which optimize & support rapid development cycle

**Step By Step :**

**Create Database** 
Create RDS for database and using PostgreSQL
1. `cd terraform/RDS`
2. `terraform init`
3. `terraform apply` 

**Deploy EKS Cluster**
Create EKS Cluster to handle microservice ( example handle 5 service, we have 4 backend service and 1 frontend service). So, i create 1 EKS cluster with 2 desired nodes for running nodes in the beginning, and set max 3 nodes for max capacity, make sure your secure EKS access only with a specific IP address, we can set allowed IP address using security group and add SG to eks cluster : example our vpn IP is 192.168.1.1 
1. `cd terraform/EKS-cluster`
2. `terraform init`
3. `terraform apply`

**Deploy Microservice**

In this section, we assume that your service has been built as docker image, and your service can connect to database.

**A. Deploy all backend services**
1. `cd EKS/backend`
2. `kubectl apply -f backend1.yaml`
3. `kubectl apply -f backend2.yaml`
4. `kubectl apply -f backend3.yaml`
5. `kubectl apply -f backend4.yaml`

**B. Deploy frontend service**
1. `cd EKS/frontend`     
2. `kubectl apply -f frontend1.yaml`
3. if frontend service can be access to public you can apply ingress `cd ingress` `kubectl apply -f ingress-frontend1.yaml`

**C. For Mobile service i have 2 option**
1. option 1 : developer can build react app on the local to generate .apk/.app/IOS app and then developer can upload to firebase distribution (for android) or upload IOS app to test flight
2. option 2 : devops can help build android app and upload automatically to firebase distribution using jenkins(example), but this option need a bigger server to run the build mobile app. 

**Logging for service**

we can capturing, analyzing, and securely storing transaction log data using opensource tools like ELK Stack, prometheus, promtail, grafana and loki. we can send service log to filebeat and then we can see the log on the kibana or grafana dashboard, and we can also make alert for log that you want to know earlier

**CI/CD**

if infra setup is complete, we need CI/CD to automate deployment process.
1. `cd deployment`
2. we have file name **.gitlab-ci.yml** it's can automatically run if developer push 
 	    to git.
